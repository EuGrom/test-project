

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Table structure for table `interns`
--

CREATE TABLE IF NOT EXISTS `interns` (
  `intern_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `f_name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `l_name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `gender` enum('M','F') CHARACTER SET utf8 DEFAULT NULL,
  `pr_st_date` date DEFAULT NULL,
  PRIMARY KEY (`intern_id`),
  KEY `ind` (`f_name`,`l_name`)
);

--
-- Dumping data for table `interns`
--

INSERT INTO `interns` (`intern_id`, `f_name`, `l_name`,`gender`, `pr_st_date`) VALUES
(1,'Ivan','Ivanov','M','2022-09-22'),
(2,'Natalia','Kurtz','F','2023-01-12'),
(3,'Kristian','Tompson','M','2023-10-20'),
(4,'Libi','Uolsh','F','2023-12-30'),
(5,'Garry','Cutcher','M','2023-01-30'),
(6,'Steven','Larson','M','2023-02-25'),
(7,'Hugh','Laurie','M','2023-03-16'),
(8,'Jonny','Walker','M','2023-01-31'),
(9,'Ketty','Perri','F','2023-05-11'),
(10,'Liza','Torn','F','2022-11-28'),
(11,'Ely','Buggle','F','2023-01-16');

--
-- Table structure for table `interns_specialty`
--

CREATE TABLE IF NOT EXISTS `interns_specialty` (
  `intern_id` smallint(10) NOT NULL,
  `specialty_id` varchar(10) CHARACTER SET utf8 NOT NULL,
  `mentor_id` int(4) NOT NULL,
  PRIMARY KEY (`intern_id`,`specialty_id`)
);

--
-- Dumping data for table `interns_specialty`
--

INSERT INTO `interns_specialty` (`intern_id`, `specialty_id`, `mentor_id`) VALUES
(1, '1', 2),
(2, '1', 1),
(3, '4', 2),
(4, '2', 3),
(5, '3', 1),
(6, '3', 3),
(7, '4', 4),
(8, '4', 4),
(9, '5', 1),
(10, '6', 3);


--
-- Table structure for table `mentors`
--

CREATE TABLE IF NOT EXISTS `mentors` (
  `mentor_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `f_name` varchar(20) NOT NULL,
  `l_name` varchar(20) NOT NULL,
  `pr_st_date` date DEFAULT NULL,
  `salary` int(11) NOT NULL,
  PRIMARY KEY (`mentor_id`)
);

--
-- Dumping data for table `mentors`
--

INSERT INTO `mentors` VALUES 
(1,'Vladimir','Zhukov','2010-01-16',1400),
(2,'Nick','Natarov','2005-05-11',1600),
(3,'Stas','Stupin','2014-12-30',1200),
(4,'Lera','Jonson','2014-01-13',1100);

--
-- Table structure for table `specialty`
--

CREATE TABLE IF NOT EXISTS `specialty` (
  `specialty_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `s_name` varchar(30) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`specialty_id`)
);

--
-- Dumping data for table `specialty`
--

INSERT INTO `specialty` VALUES (1,'QA'),(2,'Automation QA'),(3,'PM'),(4,'.Net Developer'),(5,'C++ Developer'),(6,'IOS Developer');
